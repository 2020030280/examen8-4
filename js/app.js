const llamandoFetch=()=>{
    const url ="/html/servicio.json";
    fetch(url)
    .then(respuesta => respuesta.json())
    .then(datos=>mostrarDatos(datos))
    .catch((reject)=>{
        console.log("Ocurrio un error");
    });

    const mostrarDatos=(data)=>{
        let total=0;
        const res = document.getElementById('tabla');


        for(let item of data){
            let ganancia= item.preciovta*item.preciocompra;
            ganancia=ganancia*item.cantidad
            if(item.idcliente%2==0){
                res.innerHTML +="<tr> <td>"+item.codigo+"</td> <td>"+item.idcliente+
                "</td> <td>"+item.descripcion+"</td><td>"+item.cantidad+"</td><td>"+item.preciovta+"</td><td>"+item.preciocompra+
                "</td><td>"+ganancia + "</td></tr>"
            }else{
                res.innerHTML +="<tr style='background-color:orange;'> <td>"+item.codigo+"</td> <td>"+item.idcliente+
                "</td> <td>"+item.descripcion+"</td><td>"+item.cantidad+"</td><td>"+item.preciovta+"</td><td>"+item.preciocompra+
                "</td><td>"+ganancia + "</td></tr><br>"
            }
            total= total + ganancia;
        }
        document.getElementById('total').innerHTML="Totales ganancias$" + total;

    }
} 
function cargar(){
    llamandoFetch();

}